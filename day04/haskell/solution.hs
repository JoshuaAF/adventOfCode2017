import Data.List
import Control.Monad

isPalindrome :: Ord a => [a] -> [a] -> Bool
isPalindrome x y = sort x == sort y

isSet :: Eq a => [a] -> Bool
isSet n = length n == (length $ nub n)

isPalindromeSet :: Ord a => [[a]] -> Bool
isPalindromeSet n = length n == (length $ nubBy isPalindrome n)

main :: IO ()
main = do
  contents <- readFile "../input.txt"
  let passphrases = map words $ lines contents
  putStrLn $ "Solution 1:" ++ (show $ length $ filter isSet passphrases)
  putStrLn $ "Solution 2:" ++ (show $ length $ filter isPalindromeSet passphrases)
