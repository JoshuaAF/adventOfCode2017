import Data.Map.Strict (Map, findWithDefault, insert)
import Data.List hiding (insert)
import Data.Maybe (fromJust)
import Data.Function (on)

data Direction  = E | S | W | N
data Coordinate = Coordinate Int Int deriving (Eq, Ord)

move :: Coordinate -> Direction -> Coordinate
move (Coordinate x y) E = Coordinate (x+1) y
move (Coordinate x y) S = Coordinate x (y-1)
move (Coordinate x y) W = Coordinate (x-1) y
move (Coordinate x y) N = Coordinate x (y+1)

spiralDirections :: [Direction]
spiralDirections = concatMap expandEdge $ zip edgeLengths directions
  where expandEdge (len, dir) = take len $ repeat dir
        edgeLengths           = concatMap (\n -> [n, n]) [1..]
        directions            = cycle [E, S, W, N]

spiralCoordinates :: [Coordinate]
spiralCoordinates = scanl move (Coordinate 0 0) spiralDirections

getNeighbors :: Coordinate -> [Coordinate]
getNeighbors (Coordinate x y) = [Coordinate (x + dx) (y + dy) | dx <- [-1..1], dy <- [-1..1], not (dx == 0 && dy == 0)]

generateSpiralValue :: Coordinate -> Map Coordinate Int -> Int
generateSpiralValue coord prevCoords
  | null prevCoords = 1 -- Center coordinate
  | otherwise       = sum neighborValues
    where neighborValues     = map getNeighborValue $ getNeighbors coord
          getNeighborValue n = findWithDefault 0 n prevCoords

generateSpiralValues :: [Coordinate] -> Map Coordinate Int -> [Int]
generateSpiralValues (coord:coords) prevCoords = value:(generateSpiralValues coords currentCoords)
  where value         = generateSpiralValue coord prevCoords
        currentCoords = insert coord value prevCoords

spiralValues :: [Int]
spiralValues = generateSpiralValues spiralCoordinates mempty

getSpiralValueAfter :: Int -> Int
getSpiralValueAfter val = fromJust $ find (> val) spiralValues

manhattanDistance :: Coordinate -> Int
manhattanDistance (Coordinate dx dy) = on (+) abs dx dy

distanceToIndex :: Int -> Int
distanceToIndex = manhattanDistance . (spiralCoordinates !!) . subtract 1

main = do
  contents <- readFile "../input.txt"
  let value = read contents
  putStrLn $ "Solution 1:" ++ (show $ distanceToIndex value)
  putStrLn $ "Solution 2:" ++ (show $ getSpiralValueAfter value)
