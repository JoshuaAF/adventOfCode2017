import Data.List.Split (splitOn)
import Data.List (foldl', scanl', delete)

type HexCoord = (Int, Int, Int)

move :: HexCoord -> String -> HexCoord
move (x, y, z) dir
  | dir == "n"  = (x,     y + 1, z - 1)
  | dir == "ne" = (x + 1, y,     z - 1)
  | dir == "se" = (x + 1, y - 1, z    )
  | dir == "s"  = (x,     y - 1, z + 1)
  | dir == "sw" = (x - 1, y,     z + 1)
  | dir == "nw" = (x - 1, y + 1, z    )
  | otherwise   = error "Invalid direction"

distanceToCenter :: HexCoord -> Int
distanceToCenter (x, y, z) = div ((abs x) + (abs y) + (abs z)) 2

main :: IO ()
main = do
  moves <- splitOn "," . delete '\n' <$> readFile "../input.txt"
  putStrLn $ "Solution 1:" ++ (show . distanceToCenter $ foldl' move (0, 0, 0) moves)
  putStrLn $ "Solution 2:" ++ (show . maximum $ distanceToCenter <$> scanl' move (0, 0, 0) moves)
