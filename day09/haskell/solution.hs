-- values  = (total, acc, inGarbage, skipNext)
type State = (Int,   Int, Bool,      Bool    )

parseChar1 :: State -> Char -> State
-- Priority 0: Cancels
parseChar1 (t, a, g,     True)  _  = (t,   a,   g,     False)
parseChar1 (t, a, g,     _   ) '!' = (t,   a,   g,     True)
-- Priority 1: Garbage
parseChar1 (t, a, True , s   ) '>' = (t,   a,   False, s)
parseChar1 (t, a, False, s   ) '<' = (t,   a,   True,  s)
parseChar1 (t, a, True , s   )  _  = (t,   a,   True,  s)
-- Priority 2: Groups
parseChar1 (t, a, g,     s   ) '{' = (t  , a+1, g,     s)
parseChar1 (t, a, g,     s   ) '}' = (t+a, a-1, g,     s)
-- Priority 3: Anything else
parseChar1 state _ = state

parseChar2 :: State -> Char -> State
-- Priority 0: Cancels
parseChar2 (t, _, g,     True)  _  = (t,   1,   g,     False)
parseChar2 (t, _, g,     _   ) '!' = (t,   1,   g,     True)
-- Priority 1: Garbage
parseChar2 (t, _, True , s   ) '>' = (t,   1,   False, s)
parseChar2 (t, _, False, s   ) '<' = (t,   1,   True,  s)
parseChar2 (t, _, True , s   )  _  = (t+1, 1,   True,  s)
-- Priority 2: Anything else
parseChar2 state _ = state

parseGroupScore :: [Char] -> Int
parseGroupScore stream = count
  where (count, _, _, _) = foldl parseChar1 (0, 0, False, False) stream

parseGarbageCount :: [Char] -> Int
parseGarbageCount stream = count
  where (count, _, _, _) = foldl parseChar2 (0, 0, False, False) stream

main :: IO ()
main = do
  contents <- readFile "../input.txt"
  let stream = head . lines $ contents
  putStrLn $ "Solution 1:" ++ (show $ parseGroupScore stream)
  putStrLn $ "Solution 2:" ++ (show $ parseGarbageCount stream)
