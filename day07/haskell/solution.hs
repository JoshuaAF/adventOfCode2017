import Data.Function (on)
import Data.List (sort, find, nubBy)
import Data.Maybe (fromJust)

-- BEGIN UTILITY FUNCTIONS --
trimCommas :: String -> String
trimCommas xs = [ x | x <- xs, x /= ',' ]

_unique_ :: Ord a => [a] -> [a]
_unique_ []          = []
_unique_ [x]         = [x]
_unique_ xs@[x1, x2] = if x1 == x2 then [] else xs
_unique_ (x1:x2:rest)
  | x1 == x2  = _unique_ $ dropWhile (== x1) rest
  | otherwise = x1:(_unique_ (x2:rest))

unique :: Ord a => [a] -> [a]
unique = _unique_ . sort
-- END UTILITY FUNCTIONS --

data PTree = PTree String Int [PTree]

parseLine :: [String] -> (String, Int, [String])
parseLine (name:weight:[])         = (name, (read weight), [])
parseLine (name:weight:_:programs) = (name, (read weight), programs)

findRootProgram :: [(String, Int, [String])] -> String
findRootProgram = head . unique . concatMap (\(a, _, as) -> a:as)

buildPTree :: [(String, Int, [String])] -> String -> PTree
buildPTree programs rootName = PTree rootName rootWeight children
  where (_, rootWeight, childNames) = fromJust $ find (\(n, _, _) -> n == rootName) programs
        children                    = map (buildPTree programs) childNames

getWeight :: PTree -> Int
getWeight (PTree _ w children) = sum $ w:childWeights
  where childWeights = map getWeight children

findCorrectWeight :: PTree -> Int
findCorrectWeight self@(PTree _ w children)
  | null uniqueGrandchildren = (if diff > sim then (-) else (+)) cw offset
  | otherwise                = findCorrectWeight child
    where childWeights         = map getWeight children
          diff                 = head . unique $ childWeights
          sim                  = fromJust $ find (/= diff) childWeights
          offset               = abs $ diff - sim
          child@(PTree _ cw g) = fromJust $ find (\y -> (getWeight y) == diff) children
          uniqueGrandchildren  = unique $ map getWeight g

main :: IO ()
main = do
  contents <- readFile "../input.txt"
  let cleanedLines = (lines . trimCommas) contents
  let programs                 = map (parseLine . words) cleanedLines
  let rootName                 = findRootProgram programs
  let tree                     = buildPTree programs rootName
  putStrLn $ "Solution 1:" ++ (show $ rootName)
  putStrLn $ "Solution 2:" ++ (show $ findCorrectWeight tree)
