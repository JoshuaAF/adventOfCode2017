import Prelude hiding (length)
import Data.Sequence

getNumberOfSteps1 :: Int -> Int -> Seq Int -> Int
getNumberOfSteps1 count i instructions
  | (i < 0) || i >= (length instructions) = count
  | otherwise                             = getNumberOfSteps1 (count+1) (i+current) newInstructions
    where current         = index instructions i
          newInstructions = update i (current+1) instructions 

getNumberOfSteps2 :: Int -> Int -> Seq Int -> Int
getNumberOfSteps2 count i instructions
  | (i < 0) || i >= (length instructions) = count
  | otherwise                             = getNumberOfSteps2 (count+1) (i+current) newInstructions
    where current         = index instructions i
          offsetAmount    = if current>=3 then -1 else 1
          newInstructions = update i (current+offsetAmount) instructions

main :: IO ()
main = do
  contents <- readFile "../input.txt"
  let instructions = fromList $ map read $ lines contents
  putStrLn $ "Solution 1:" ++ (show $ getNumberOfSteps1 0 0 instructions)
  putStrLn $ "Solution 2:" ++ (show $ getNumberOfSteps2 0 0 instructions)
