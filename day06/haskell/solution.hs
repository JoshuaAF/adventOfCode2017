import Data.Sequence (Seq)
import qualified Data.Sequence as Seq

import Data.Set (Set)
import qualified Data.Set as Set

import Data.List

distribute :: Int -> Int -> Seq Int -> Seq Int
distribute 0         _            banks = banks
distribute remaining currentIndex banks = distribute newRemaining newIndex newBanks
  where newRemaining = remaining - 1
        newIndex     = mod (currentIndex + 1) (length banks)
        newBanks     = Seq.adjust (+1) currentIndex banks

doCycle :: Seq Int -> Seq Int
doCycle banks = distribute largestBank startIndex withoutLargest
  where largestBank    = maximum banks
        bankIndex      = head $ Seq.elemIndicesL largestBank banks
        startIndex     = mod (bankIndex + 1) (length banks)
        withoutLargest = Seq.update bankIndex 0 banks

getFirstRepeatedConfig :: Int -> Seq Int -> Set (Seq Int) -> (Int, Seq Int)
getFirstRepeatedConfig count banks prevConfigs
  | Set.member banks prevConfigs = (count, banks)
  | otherwise                    = getFirstRepeatedConfig newCount newBanks newPrevConfigs
    where newCount       = count + 1
          newBanks       = doCycle banks
          newPrevConfigs = Set.insert banks prevConfigs
          
getTimeToCycle :: Int -> Seq Int -> Seq Int -> Int
getTimeToCycle count banks targetConfig
  | banks == targetConfig = count
  | otherwise             = getTimeToCycle newCount newBanks targetConfig
    where newCount = count + 1
          newBanks = doCycle banks

main :: IO ()
main = do
  contents <- readFile "../input.txt"
  let banks                   = Seq.fromList $ map read $ words contents
  let (cycleTime, banksAfter) = getFirstRepeatedConfig 0 banks Set.empty
  let timeToNextCycle         = getTimeToCycle 1 (doCycle banksAfter) (banksAfter)
  putStrLn $ "Solution 1:" ++ (show cycleTime)
  putStrLn $ "Solution 2:" ++ (show timeToNextCycle)
