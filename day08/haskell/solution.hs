import Data.Maybe (fromMaybe)
import qualified Data.Map.Strict as Map
import Data.Map.Strict (Map)

type UpdateOp  = (Int -> Int -> Int)
type CheckOp   = (Int -> Int -> Bool)
type Condition = (String, CheckOp, Int)
type Operation = (String, UpdateOp, Int)

data Instruction = Instruction Operation Condition

toCheckOp :: String -> CheckOp
toCheckOp s = case s of
  "==" -> (==)
  "!=" -> (/=)
  "<=" -> (<=)
  ">=" -> (>=)
  "<"  -> (<)
  ">"  -> (>)

toUpdateOp :: String -> UpdateOp
toUpdateOp s = case s of
  "inc" -> (+)
  "dec" -> (-)

toInstruction :: [String] -> Instruction
toInstruction [reg1, op1, amt1, _, reg2, op2, amt2] = Instruction updatePart checkPart
  where updatePart  = (reg1, toUpdateOp op1, read amt1)
        checkPart   = (reg2, toCheckOp op2, read amt2)

doInstruction :: Map String Int -> Instruction -> Map String Int
doInstruction registers (Instruction (updateReg, updateOp, updateAmt) (checkReg, checkOp, checkAmt))
  | checkOp (Map.findWithDefault 0 checkReg registers) checkAmt = newRegisters
  | otherwise                                                   = registers
    where newRegisters = Map.alter doUpdate updateReg registers
          doUpdate     = \reg -> Just (updateOp (fromMaybe 0 reg) updateAmt)

main :: IO ()
main = do
  contents <- readFile "../input.txt"
  let instructions   = map (toInstruction . words) $ lines contents
  let registerStates = scanl doInstruction Map.empty instructions
  putStrLn $ "Solution 1:" ++ (show . maximum . Map.elems . last $ registerStates)
  putStrLn $ "Solution 2:" ++ (show . maximum . (concatMap Map.elems) $ registerStates)
