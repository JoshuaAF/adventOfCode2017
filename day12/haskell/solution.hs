{-# LANGUAGE ViewPatterns #-}

import Data.List (find)
import Data.Maybe (fromJust)
import Data.Graph (graphFromEdges, scc)
import Data.Tree (flatten)

parseLine :: String -> (Int, Int, [Int])
parseLine (words -> (id:_:neighbors)) = (read id, read id, read <$> neighbors)

main = do
  contents <- readFile "../input.txt"
  let programs = parseLine . filter (/= ',') <$> lines contents
  let (graph, _, _) = graphFromEdges programs
  let components = flatten <$> scc graph
  let component = fromJust . find (elem 0) $ components
  putStrLn $ "Solution 1:" ++ (show $ length component)
  putStrLn $ "Solution 2:" ++ (show $ length components)
