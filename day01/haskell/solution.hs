import Data.Char

getNeighborAtOffset :: [Int] -> Int -> Int -> Int
getNeighborAtOffset list index offset = list !! ((index + offset) `mod` (length list))

getValueOfIndex :: [Int] -> Int -> Int -> Int
getValueOfIndex list index offset
  | value == neighbor = value
  | otherwise         = 0
    where value    = list !! index
          neighbor = getNeighborAtOffset list index offset

main :: IO ()
main = do
  contents <- readFile "../input.txt"
  let numbers = map digitToInt contents
  let len = length numbers
  putStrLn $ "Solution 1:" ++ (show $ sum $ map (\i -> getValueOfIndex numbers i 1) [0..len-1])
  putStrLn $ "Solution 2:" ++ (show $ sum $ map (\i -> getValueOfIndex numbers i (div len 2)) [0..len-1])