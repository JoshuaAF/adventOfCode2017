use std::io::prelude::*;
use std::fs::File;

fn matches_neighbor(list: &Vec<u32>, index: usize, offset: usize) -> bool {
    return list[index] == list[(index + offset) % list.len()];
}

fn main() {
    let mut f = File::open("../input.txt")
        .expect("file not found");

    let mut contents = String::new();
    f.read_to_string(&mut contents)
        .expect("error reading file");

    const RADIX: u32 = 10;
    let numbers: Vec<u32> = contents
        .chars()
        .into_iter()
        .map(|c| c.to_digit(RADIX).unwrap())
        .collect();

    let mut solution1: u32 = 0;
    let mut solution2: u32 = 0;
    for (index, value) in numbers.iter().enumerate() {
        if matches_neighbor(&numbers, index, 1) {
            solution1 += *value;
        }
        if matches_neighbor(&numbers, index, numbers.len() / 2) {
            solution2 += *value;
        }
    }

    println!("Solution 1:{}", solution1);
    println!("Solution 2:{}", solution2);
}
