import Data.List.Split (splitOn)
import Data.Maybe (fromJust)
import Data.List (findIndex)

type Depth = Int
type Layer = Int

type Scanner = (Layer, Depth)

readLine :: [String] -> Scanner
readLine [layer, depth] = (read layer, read depth)

isCaught :: Int -> Scanner -> Bool
isCaught delay (layer, depth) = (==) 0 $ mod (delay + layer) (2 * (depth - 1))

anyCaught :: Int -> [Scanner] -> Bool
anyCaught delay scanners = any (isCaught delay) scanners

cost :: Int -> Scanner -> Int
cost delay scanner@(layer, depth)
  | isCaught delay scanner = layer * depth
  | otherwise              = 0

costs :: Int -> [Scanner] -> [Int]
costs delay scanners = cost delay <$> scanners

main :: IO ()
main = do
  contents <- readFile "../input.txt"
  let layers    = readLine . splitOn ": " <$> lines contents
  let totalCost = sum $ costs 0 layers
  let attempts  = (\y -> anyCaught y layers) <$> [0..]
  putStrLn $ "Solution 1:" ++ (show totalCost)
  putStrLn $ "Solution 2:" ++ (show . fromJust $ findIndex not attempts)
