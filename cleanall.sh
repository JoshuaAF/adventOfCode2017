shopt -s globstar
origdir="$PWD"
for i in **/; do
  cd "$i"
  if [ -f "Makefile" ]; then make clean; fi
  cd "$origdir"
done
