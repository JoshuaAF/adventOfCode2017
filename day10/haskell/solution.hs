import qualified Data.List.Split as Split
import qualified Data.List as List
import qualified Data.Bits as Bits
import qualified Text.Printf as P
import Data.Int

type RopeState = ([Int], Int, Int)

rotateList :: Int -> [a] -> [a]
rotateList i xs = take count . drop (mod i count) . cycle $ xs
  where count = length xs

twistSegment :: RopeState -> Int -> RopeState
twistSegment (values, index, skipSize) len = (unrotated, newIndex, skipSize + 1)
    where rotated         = rotateList index values
          (toTwist, rest) = splitAt len rotated
          twisted         = (reverse toTwist) ++ rest
          unrotated       = rotateList (negate index) twisted
          newIndex        = mod (index + len + skipSize) (length values)

performTwists :: [Int] -> [Int]
performTwists lengths = vals
  where (vals, _, _) = foldl twistSegment ([0..255], 0, 0) lengths

xorInts :: [Int] -> Int
xorInts vals = fromIntegral $ foldl1 Bits.xor bitValues
  where bitValues = (fromIntegral <$> vals)::[Int32]

denseHash :: [Int] -> String
denseHash ns = foldl1 (++) hexDigits
  where hexDigits = P.printf "%02x" . xorInts <$> Split.chunksOf 16 ns

main :: IO ()
main = do
  contents <- List.delete '\n' <$> readFile "../input.txt"
  let lengths = read <$> Split.splitOn "," contents
  let prod = product . take 2 . performTwists $ lengths
  let lengths' = (fromEnum <$> contents) ++ [17, 31, 73, 47, 23]
  let hash = denseHash . performTwists . take ((*) 64 $ length lengths') $ cycle lengths'
  putStrLn $ "Solution 1:" ++ (show prod)
  putStrLn $ "Solution 2:" ++ hash
